package com.ghn.venuesearch.view

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.ghn.venuesearch.R
import com.ghn.venuesearch.models.Venue
import com.ghn.venuesearch.state.ItemListViewState
import com.ghn.venuesearch.util.VENUES_EXTRA
import com.ghn.venuesearch.util.VENUE_EXTRA
import com.ghn.venuesearch.util.getViewModel
import com.ghn.venuesearch.viewModel.MainActivityViewModel
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), MainActivityViewModel.UpdateListener,
        ItemAdapter.ItemAdapterListener {

    private var adapter: ItemAdapter? = null
    private val compositeDisposable = CompositeDisposable()
    private val viewModel: MainActivityViewModel by lazy { getViewModel<MainActivityViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        initViewModel()
    }

    private fun setupViews() {
        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_menu)
        }
        adapter = ItemAdapter(this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapter

        mainFab.setOnClickListener { onFabItemClicked() }
    }

    private fun initViewModel() {
        viewModel.setStateUpdateListener(this)

        //if using LiveData getPlaces().observe(this, { places})

        //handles listening of text input changes to start performing search against API
        val disposable = mainSearchText.textChanges()
                .skip(1)
                .map { it.toString() }
                .distinctUntilChanged()
                .filter { it.isNotEmpty() }
                .doOnNext { mainProgressbar.visibility = View.VISIBLE }
                .debounce(600, TimeUnit.MILLISECONDS)
                .subscribe({ viewModel.runQuery(it) }, { Timber.e(it) })
        compositeDisposable.add(disposable)
    }

    override fun onUpdate(state: ItemListViewState) {
        mainProgressbar.visibility = View.GONE
        adapter?.update(state.items)
        mainFab.visibility = if (state.items.isNotEmpty()) View.VISIBLE else View.GONE

        if (state.items.isEmpty()  && state.toolbarTitle.startsWith("Error"))
            Snackbar.make(container, getString(R.string.networkError), Snackbar.LENGTH_SHORT).show()
        else
            if (state.toolbarTitle.isNotEmpty()) toolbar.title = state.toolbarTitle
            else toolbar.title = getString(R.string.venue_search)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        viewModel.unSubscribeFromObservable()
    }

    override fun onResume() {
        super.onResume()
        adapter?.getItems()?.let { list ->
            val items = list.map {
                it.venue.isFavorite = viewModel.getFavoritePrefs(it.venue.id)
                return@map it
            }
            adapter?.update(items)
        }
    }

    override fun onItemClicked(venue: Venue) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(VENUE_EXTRA, venue)
        startActivity(intent)
    }

    private fun onFabItemClicked() {
        adapter?.getItems()?.isNotEmpty().let {
            val intent = Intent(this, MapsActivity::class.java)
            val venues: ArrayList<Venue>? = ArrayList(adapter?.getItems()?.map { it.venue })
            intent.putParcelableArrayListExtra(VENUES_EXTRA, venues)
            startActivity(intent)
        }
    }

    override fun onFavoriteClicked(venue: Venue, checked: Boolean) {
        viewModel.applyFavoriteToPrefs(venue, checked)
    }

}
