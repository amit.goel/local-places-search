package com.ghn.venuesearch.view

import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ghn.venuesearch.R
import com.ghn.venuesearch.databinding.ActivityDetailsBinding
import com.ghn.venuesearch.models.Location
import com.ghn.venuesearch.models.Venue
import com.ghn.venuesearch.util.START_COORDINATES
import com.ghn.venuesearch.util.VENUE_EXTRA
import com.ghn.venuesearch.util.convertDpToPixel
import com.ghn.venuesearch.util.getViewModel
import com.ghn.venuesearch.viewModel.DetailActivityViewModel
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.content_scrolling.*

class DetailsActivity : AppCompatActivity(), DetailActivityViewModel.UpdateListener {

    private val viewModel: DetailActivityViewModel by lazy { getViewModel<DetailActivityViewModel>() }

    private lateinit var binding: ActivityDetailsBinding
    private val mapsUrl = "https://maps.googleapis.com/maps/api/staticmap?center=$START_COORDINATES" +
            "&key=AIzaSyC3Pkuzn9tj47XulpIZ0xOBXzVaBsCe2UI&maptype=roadmap&" +
            "markers=color:red%7Clabel:C%7C$START_COORDINATES&"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val venue = intent.getParcelableExtra<Venue>(VENUE_EXTRA)
        setBindings(venue)
        viewModel.setStateUpdateListener(this)
        viewModel.runQuery(venue.id)
    }

    private fun initToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_layout.setExpandedTitleColor(Color.parseColor("#00000000"))
    }

    private fun setBindings(venue: Venue) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        binding.apply {
            item = venue
            mapsUrl = getUrl(venue)
            handlers = this@DetailsActivity
        }

        initToolBar()
        favoriteToggle.setOnCheckedChangeListener { cb, checked ->
            if (cb.isPressed) toggleFavorite(venue, checked)
        }
    }

    fun handleClick(viewId: Int, venue: Venue) {
        val intent: Intent? = when (viewId) {
            R.id.fab -> Intent(Intent.ACTION_VIEW, getMapsUri(venue.location, venue.name))
            R.id.phone -> Intent(Intent.ACTION_DIAL, Uri.parse("tel:${venue.contact.phone}"))
            R.id.twitter -> Intent(Intent.ACTION_VIEW, Uri.parse(venue.contact.getTwitterUrl()))
            R.id.web -> Intent(Intent.ACTION_VIEW, Uri.parse(venue.url))
            R.id.ig -> Intent(Intent.ACTION_VIEW, Uri.parse(venue.contact.getIgUrl()))
            else -> null
        }
        intent?.let { startActivity(it) }
    }

    private fun getMapsUri(location: Location, name: String): Uri =
            Uri.parse("geo:0,0?q=${location.lat},${location.lng}($name)")

    private fun toggleFavorite(venue: Venue, checked: Boolean) {
        // was not able to do two way data bind on kotlin data class, using BaseObservable
        // and am forced to update items directly.
        venue.isFavorite = checked
        viewModel.applyFavoriteToPrefs(venue)
    }

    private fun getUrl(venue: Venue): String {
        val (h, w) = convertDpToPixel(360F).toInt() to resources.displayMetrics.widthPixels
        return "${mapsUrl}markers=color:blue%7Clabel:S%7C${venue.location.getCoordinates()}&size=300x300&scale=2"
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.unSubscribeFromObservable()
    }

    override fun onUpdate(venue: Venue) {
        binding.item = venue
    }

}
