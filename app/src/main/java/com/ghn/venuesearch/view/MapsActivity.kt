package com.ghn.venuesearch.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ghn.venuesearch.R
import com.ghn.venuesearch.util.VENUE_EXTRA
import com.ghn.venuesearch.models.Venue
import com.ghn.venuesearch.util.getWidthHeight
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        handleMapInit(intent.getParcelableArrayListExtra<Venue>("venues"))
    }

    private fun handleMapInit(venues: ArrayList<Venue>) {
        val builder = LatLngBounds.Builder()
        venues.forEach { builder.include(makeMarker(it)?.position) }
        val cu: CameraUpdate? = getCameraUpdate(builder)
        setLocalPosition(cu)
        mMap.setOnInfoWindowClickListener { handleClick(it.tag as Venue) }
    }

    private fun getCameraUpdate(builder: LatLngBounds.Builder): CameraUpdate? {
        val bounds = builder.build()
        val (width, height) = resources.displayMetrics.getWidthHeight()
        val padding = (height * 0.12).toInt()
        return CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)
    }

    private fun setLocalPosition(cu: CameraUpdate?) {
        val coordinate = LatLng(47.6062, -122.3321)
        mMap.addCircle(CircleOptions().center(coordinate)
                .radius(30.0).strokeColor(Color.GREEN).fillColor(Color.BLUE))
        mMap.animateCamera(cu)
    }

    private fun makeMarker(venue: Venue): Marker? {
        val location = LatLng(venue.location.lat, venue.location.lng)
        val marker = mMap.addMarker(MarkerOptions().position(location).title(venue.name))
        marker.tag = venue
        return marker
    }

    private fun handleClick(venue: Venue) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(VENUE_EXTRA, venue)
        startActivity(intent)
    }
}
