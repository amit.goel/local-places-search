package com.ghn.venuesearch.repository

import android.arch.lifecycle.MutableLiveData
import com.ghn.venuesearch.models.FSResponse

interface Repository {
    fun getPlaces(): MutableLiveData<List<FSResponse>>
}