package com.ghn.venuesearch.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.ghn.venuesearch.HomeAwayApplication
import com.ghn.venuesearch.models.FSResponse
import com.ghn.venuesearch.models.Venue
import com.ghn.venuesearch.repository.PlaceRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.Nullable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class DetailActivityViewModel : ViewModel() {

    @Inject
    lateinit var repository: PlaceRepository
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var stores: LiveData<List<FSResponse>>? = null
        private set

    private var listener: UpdateListener? = null

    interface UpdateListener {
        fun onUpdate(venue: Venue)
    }

    init {
        initializeDagger()
    }

    fun runQuery(id: String) {
        val disposable = repository.getPlaceApi(id)
                .map { it.body() }
                .map { it.venue.venue }
                .retry(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ listener?.onUpdate(it) }, { Timber.e(it) })
        compositeDisposable.add(disposable)
    }

    private fun initializeDagger() {
        HomeAwayApplication.appComponent.inject(this)
    }

    fun unSubscribeFromObservable() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun setStateUpdateListener(@Nullable listener: UpdateListener?) {
        this.listener = listener
    }

    fun applyFavoriteToPrefs(venue: Venue) {
        repository.writePrefs(venue.id, venue.isFavorite)
    }

}
