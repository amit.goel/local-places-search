package com.ghn.venuesearch.viewModel

import android.arch.lifecycle.ViewModel
import com.ghn.venuesearch.HomeAwayApplication
import com.ghn.venuesearch.models.Venue
import com.ghn.venuesearch.repository.PlaceRepository
import com.ghn.venuesearch.state.ItemListViewState
import com.ghn.venuesearch.state.ItemRow
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.Nullable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MainActivityViewModel : ViewModel() {

    @Inject
    lateinit var repository: PlaceRepository

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

   /* var places: LiveData<List<FSResponse>>? = null
        private set*/

    private var listener: UpdateListener? = null

    interface UpdateListener {
        fun onUpdate(state: ItemListViewState)
    }

    init {
        initializeDagger()
        //loadPlaces()
    }

    fun runQuery(query: String) {
        val disposable = repository.getPlacesApi(query)
                .map { it.body() }
                .flatMapIterable { it.venues.venues }
                .doOnNext { it.isFavorite = getFavoritePrefs(it.id) }
                .toList()
                .retry(2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { listener?.onUpdate(ItemListViewState("Places for $query", covertPlaces(it))) },
                        { listener?.onUpdate(ItemListViewState("Error Thrown", listOf()))
                            Timber.e(it) }
                )
        compositeDisposable.add(disposable)
    }

    fun getFavoritePrefs(id: String): Boolean  = repository.getPrefsFavorite(id)

    private fun covertPlaces(list: List<Venue>): List<ItemRow> = list.map { ItemRow(it) }

    private fun initializeDagger() {
        HomeAwayApplication.appComponent.inject(this)
    }

    fun setStateUpdateListener(@Nullable listener: UpdateListener?) {
        this.listener = listener
        listener?.onUpdate(ItemListViewState("", listOf()))
    }

    fun unSubscribeFromObservable() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    /*private fun loadPlaces() {
        if (places == null) {
            places = MutableLiveData<List<FSResponse>>()
            // places = repository.getPlaces()
        }
    }*/

    fun applyFavoriteToPrefs(venue: Venue, checked: Boolean) {
        venue.isFavorite = checked
        repository.writePrefs(venue.id, checked)
    }
}
