package com.ghn.venuesearch.models

import android.os.Parcelable
import com.ghn.venuesearch.util.distance
import com.ghn.venuesearch.util.formatPhoneNumber
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class FSResponse(@SerializedName("response") var venues: Venues)
data class VenueResponse(@SerializedName("response") var venue: Venue2)

data class Venues(val venues: List<Venue>)
data class Venue2(val venue: Venue)

@Parcelize
data class Venue(val id: String,
                 val name: String,
                 val location: Location,
                 val contact: Contact,
                 val categories: List<Category>,
                 val rating: String?,
                 val ratingColor: String?,
                 val url: String?,
                 val hours: Hours?,
                 val bestPhoto: Photo?,
                 var isFavorite: Boolean = false) : Parcelable {
    fun getUrlFormat() = "Web: ${url ?: "Unavailable"}"
    fun getCategoryName() = if (categories.isNotEmpty()) categories[0].name else ""
    fun getCategoryPicUrl() = if (categories.isNotEmpty()) categories[0].icon.getUrl() else ""
}

@Parcelize
data class Location(val address: String?,
                    val lat: Double,
                    val lng: Double,
                    val city: String?,
                    val state: String?,
                    val postalCode: String?) : Parcelable {

    fun getDist() = "Distance: ${String.format("%.2f", distance(lat2 = lat, lon2 = lng))} miles"
    fun getCoordinates() = "$lat,$lng"
    fun getAddressLine2() = "$city, $state $postalCode"
}

@Parcelize
data class Category(val name: String,
                    val icon: Icon) : Parcelable

@Parcelize
data class Icon(val prefix: String, val suffix: String) : Parcelable {
    fun getUrl() = "${prefix}32${suffix}"
}

@Parcelize
data class Contact(val phone: String?, val twitter: String?, val instagram: String?) : Parcelable{
    fun getPhoneFormat() = "Phone: ${formatPhoneNumber(phone)}"
    fun getTwitterFormat() = "Twitter: ${twitter ?: "Unavailable"}"
    fun getInstagramFormat() = "Instagram: ${instagram ?: "Unavailable"}"
    fun getIgUrl(): String?  = "http://instagram.com/_u/$instagram"
    fun getTwitterUrl(): String?  = "https://twitter.com/$twitter"
}

@Parcelize
data class Hours(val status: String?) : Parcelable

@Parcelize
data class Photo(val prefix: String?,
                 val suffix: String?) : Parcelable {
    fun getUrl() = "${prefix}cap300$suffix"
}