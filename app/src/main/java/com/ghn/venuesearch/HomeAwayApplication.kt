package com.ghn.venuesearch

import android.support.multidex.MultiDexApplication
import com.ghn.venuesearch.di.*
import com.ghn.venuesearch.timber.NotLoggingTree
import timber.log.Timber

class HomeAwayApplication : MultiDexApplication() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        else Timber.plant(NotLoggingTree())

        initializeDagger()
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .prefsModule(PrefsModule())
                .networkModule(NetworkModule())
                .build()
    }
}