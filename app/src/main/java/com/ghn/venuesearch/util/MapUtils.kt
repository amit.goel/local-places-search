package com.ghn.venuesearch.util

import android.util.DisplayMetrics

const val LATITUDE_DEFAULT = 47.6062
const val LONGITUDE_DEFAULT = -122.3321

const val LOCATION_NEAR = "Seattle, Washington"
const val START_COORDINATES = "$LATITUDE_DEFAULT,$LONGITUDE_DEFAULT"

fun distance(lat1: Double = LATITUDE_DEFAULT, lon1: Double = LONGITUDE_DEFAULT,
             lat2: Double, lon2: Double, unit: Char = 'M'): Double {
    val theta = lon1 - lon2
    var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta))
    dist = Math.acos(dist)
    dist = rad2deg(dist)
    dist *= 60.0 * 1.1515
    return when (unit) {
        'K' -> dist * 1.609344
        'N' -> dist * 0.8684
        else -> dist
    }
}

private fun deg2rad(deg: Double): Double = deg * Math.PI / 180.0

private fun rad2deg(rad: Double): Double = rad * 180.0 / Math.PI

fun DisplayMetrics.getWidthHeight(): Pair<Int, Int> = this.widthPixels to this.heightPixels